// #include <GL/glew.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <AntTweakBar.h>
#include "linmath.h"
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
static const struct
{
    float x, y;
    float r, g, b;
} vertices[3] =
{
    { -0.6f, -0.4f, 1.f, 0.f, 0.f },
    {  0.6f, -0.4f, 0.f, 1.f, 0.f },
    {   0.f,  0.6f, 0.f, 0.f, 1.f }
};
static const char* vertex_shader_text =
"uniform mat4 MVP;\n"
"attribute vec3 vCol;\n"
"attribute vec2 vPos;\n"
"varying vec3 color;\n"
"void main()\n"
"{\n"
"    gl_Position = MVP * vec4(vPos, 0.0, 1.0);\n"
"    color = vCol;\n"
"}\n";
static const char* fragment_shader_text =
"varying vec3 color;\n"
"void main()\n"
"{\n"
"    gl_FragColor = vec4(color, 1.0);\n"
"}\n";
static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}
static void TW_CALL tw_cb(void * clientData)
{ 
    printf("Hoge!\n");
}
bool tw_val_bool = false;
int tw_val_int = 0;
float tw_val_float = 0;
float tw_val_color3f[3] = {0,0,0};
float tw_val_quat4f[4] = {1,0,0,0};
float tw_val_dir3f[3] = {1,0,0};

static void window_size_callback(GLFWwindow* window, int width, int height)
{
    TwWindowSize(width, height);
}

int getDevicePixelRatio(GLFWwindow* window) {
  int wWidth, wHeight;
  glfwGetWindowSize(window, &wWidth, &wHeight);
  int fbWidth, fbHeight;
  glfwGetFramebufferSize(window, &fbWidth, &fbHeight);
  return fbWidth/wWidth;
}

int main(void)
{
    GLFWwindow* window;
    GLuint vertex_buffer, vertex_shader, fragment_shader, program;
    GLint mvp_location, vpos_location, vcol_location;
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
        exit(EXIT_FAILURE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    window = glfwCreateWindow(400, 300, "Simple example", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwSetWindowSizeCallback(window, window_size_callback);
    glfwSetWindowSize(window, 640, 480);

    glfwSetMouseButtonCallback(window, (GLFWmousebuttonfun)TwEventMouseButtonGLFW3);
    glfwSetCursorPosCallback(window, (GLFWcursorposfun)TwEventCursorPosGLFW3);
    glfwSetScrollCallback(window, (GLFWscrollfun)TwEventScrollGLFW3);
    glfwSetKeyCallback(window, (GLFWkeyfun)TwEventKeyGLFW3);
    glfwSetCharCallback(window, (GLFWcharfun)TwEventCharModsGLFW3);

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);
    // NOTE: OpenGL error checks have been omitted for brevity
    glGenBuffers(1, &vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_shader_text, NULL);
    glCompileShader(vertex_shader);
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_shader_text, NULL);
    glCompileShader(fragment_shader);
    program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);
    mvp_location = glGetUniformLocation(program, "MVP");
    vpos_location = glGetAttribLocation(program, "vPos");
    vcol_location = glGetAttribLocation(program, "vCol");
    glEnableVertexAttribArray(vpos_location);
    glVertexAttribPointer(vpos_location, 2, GL_FLOAT, GL_FALSE,
                          sizeof(float) * 5, (void*) 0);
    glEnableVertexAttribArray(vcol_location);
    glVertexAttribPointer(vcol_location, 3, GL_FLOAT, GL_FALSE,
                          sizeof(float) * 5, (void*) (sizeof(float) * 2));


    // set fontscaling for anttweakbar to conform with high dpi displays (IMPORTANT: TwDefine must be called before TwInit!)
    std::stringstream twStream;
    twStream << " GLOBAL fontscaling=" << getDevicePixelRatio(window) << ' ';
    TwDefine(twStream.str().c_str());
    // since the fontscaling is set globally only once, moving the window to another display with different pixel ratio
    // could break the bar functionality - TO CHECK - possible overcome: re-init the bar after a display change

    TwInit(TW_OPENGL, NULL);
    TwBar *bar = TwNewBar("TweakBar");
    TwDefine("GLOBAL fontsize=2");
    TwAddButton(bar, "Button 1", tw_cb, NULL, " label='Life is like a box a chocolates' ");
    TwAddVarRW(bar, "val_bool", TW_TYPE_BOOLCPP, &tw_val_bool, " group=Engine label='Bool value' ");
    TwAddVarRW(bar, "val_int32", TW_TYPE_INT32, &tw_val_int, " min=-10 max=10 group=Engine label='Int value' ");
    TwAddVarRW(bar, "val_float", TW_TYPE_FLOAT, &tw_val_float, " min=-10 max=10 step=0.01 group=Engine label='Float value' ");
    TwAddVarRW(bar, "val_color3f", TW_TYPE_COLOR3F, tw_val_color3f, " group=Engine label='color value' ");
    // TwAddVarRW(bar, "val_stdstr", TW_TYPE_STDSTRING, &tw_val_, " min=-10 max=10 group=Engine label=' value' ");
    TwAddVarRW(bar, "val_quat4f", TW_TYPE_QUAT4F, tw_val_quat4f, " group=Engine label='quat value' ");
    TwAddVarRW(bar, "val_dir3f", TW_TYPE_DIR3F, tw_val_dir3f, "group=Engine label='dir value' ");
/*
    TW_TYPE_BOOLCPP = 1,
    TW_TYPE_INT32,
    TW_TYPE_FLOAT,
    TW_TYPE_COLOR3F,    // 3 floats color. Order is RGB.
    TW_TYPE_STDSTRING = (0x2ffe0000+sizeof(std::string)),  // VS2010 C++ STL string (std::string)
    TW_TYPE_QUAT4F = TW_TYPE_CDSTRING+2, // 4 floats encoding a quaternion {qx,qy,qz,qs}
    TW_TYPE_DIR3F,      // direction vector represented by 3 floats
*/
    TwDefine(" GLOBAL help='This example shows how to integrate AntTweakBar with GLFW and OpenGL.' "); // Message added to the help bar.

    while (!glfwWindowShouldClose(window))
    {
        float ratio;
        int width, height;
        mat4x4 m, p, mvp;
        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;
        glViewport(0, 0, width, height);
        glClearColor(0, 0, 1, 0);
        glClear(GL_COLOR_BUFFER_BIT);
        mat4x4_identity(m);
        mat4x4_rotate_Z(m, m, (float) glfwGetTime());
        mat4x4_ortho(p, -ratio, ratio, -1.f, 1.f, 1.f, -1.f);
        mat4x4_mul(mvp, p, m);
        glUseProgram(program);
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
        glUniformMatrix4fv(mvp_location, 1, GL_FALSE, (const GLfloat*) mvp);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glUseProgram(0);
        TwDraw();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();
    TwTerminate();
    exit(EXIT_SUCCESS);
}
